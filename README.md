# Docu base

Documentation is helpfully and necessary, but no one wants to do it. I think one reason is because it takes so much time to handle the different formats and most of it is done manually.

## Some problems 

You create simple file based documentations in your repositories, like this markdown file for example.

If you want to share it with others, which haven't access to the repository, you need to sent it to them.\
In the most simple case, it is only one file. But could also be the file itself and a bunch of media files.

Another solution are wikis. But normally they use their own syntax.\
For example [Confluence](https://en.wikipedia.org/w/index.php?title=Confluence_(software)&oldid=991986066).\
There are add-ons to insert markdown content, but once inserted, you can't edit it as markdown anymore or extract it again as markdown.

## A possible solution?

Let's try this. We create a simple base version of the content and use common tools to convert it in the different formats.

So the way will always be:
* Create and edit the docu in the repository.
* Use tools to convert it to the needed format.
* Only update it in the wiki from the repository source files, in the proper format.

## Preparations

### Fontx

Install a free replacement for the Tahoma font.

... (todo, how to install at linux).

See [https://en.wikipedia.org/w/index.php?title=Liberation_fonts&oldid=996051189] and [https://github.com/liberationfonts/liberation-fonts/releases].

Download the "*-ttf.*" file and extract the .ttf files.

At Windows 10, install the font by double click at the .ttf file.

A good replace for Tahoma is "LiberationSans-Regular.ttf".

### Libre office

See [https://en.wikipedia.org/w/index.php?title=LibreOffice&oldid=1002376058].

### Pandoc

Install pandoc for format converting.

```bash
sudo apt install pandoc
which pandoc
pandoc --version
```
