# How to?

## Markdown to PDF

The initial plan was to use pandoc. This does work for the content, but styling the PDF makes trouble. Especially to set the font family for the PDF.

So the workaround is to use the Libre Office Writer.

* Create the Markdown file (*.md).
* Convert to Libre Office Writer file (*.odt), with pandoc.

```bash
# pandoc [input-file].md -o [outpu-file].odt
pandoc example/Duck-or-rabbit.base.md -o example/duck-or-rabbit.odt
```

* Open the .odt file with Libre Office Writer.
* Select all text and change the font to "Liberation Sans" and save the file.
* Export the .odt file as PDF.

### Export as PDF

* In Libre Office Writer, it should be in the file menu, german "Datei".
* File > '(something) PDF (something)'.
* German: Datai > Als PDF exportieren.
* You should export it as "PDF/A".