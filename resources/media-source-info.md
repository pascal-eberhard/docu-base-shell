# Media source info

## resources/images/duck-or-rabbit.png

```yaml
attribution: "Taro Istok, CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons"
file: "resources/images/duck-or-rabbit.png"
fileUrl: "https://upload.wikimedia.org/wikipedia/commons/d/d3/Duck%2C_Rabbit%21_Duck%21.svg"
from: "Wikipedia/Wikimedia commons"
pageUrl: "https://commons.wikimedia.org/wiki/File:Duck,_Rabbit!_Duck!.svg"
originalEdited: true
```
