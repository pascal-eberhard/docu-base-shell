# Apache web server

## Add virtual host

(Especially at Debian Linux at Windows WSL).

### The config file

Create a new config file the virtual host.

For example "web-localhost.conf".

```plain
<VirtualHost *:80>
  CustomLog [path-to-logs]/access.log combined
  DocumentRoot "[document-root_no-tailing-slash]"
  ErrorLog [path-to-logs]/error.log
  ServerAdmin [email, e.g. "admin@localhost"]
  ServerName web.localhost
   
  <Directory "[document-root_no-tailing-slash]/">
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
  </Directory>
</VirtualHost>
```

Correct the file and user permissions at the document root.

```bash
sudo chown -R $USER:$USER [document-root_no-tailing-slash]/
sudo chmod -R 755 [document-root_no-tailing-slash]/
```

### Activate the virtual host

In this example we use Linux via Windows WSL. It is more easy to keep the config files somewhere else, to edit them with Windows programs.

At real Linux, just ignore the config file copy steps.

```bash
# Copy the config file to the apache config files folder
sudo cp -f [config-file-folder]web-localhost.conf /etc/apache2/sites-available/

# Check the Syntax
sudo apache2ctl configtest

# Activate the config
sudo a2ensite web-localhost.conf

# Disable the default config (only needed at first time, after the apache installation)
sudo a2dissite 000-default.conf

# Reload the server, activate the changes
sudo service apache2 reload
```

Without changes at the Windows hosts file, the virtual host should now be reachable with browser via "http://web.localhost/". Surly you only see something, if you place an index file (e.g. "index.html") in the document root.

## Edit virtual host

Edit the config file.

(Especially at Debian Linux at Windows WSL).

Correct the file and user permissions at the document root. (Only if the document root was moved).

```bash
sudo chown -R $USER:$USER [document-root_no-tailing-slash]/
sudo chmod -R 755 [document-root_no-tailing-slash]/
```

### Re-activate the virtual host

In this example we use Linux via Windows WSL. It is more easy to keep the config files somewhere else, to edit them with Windows programs.

At real Linux, just ignore the config file copy steps.

```bash
# Copy the config file to the apache config files folder
sudo cp -f [config-file-folder]web-localhost.conf /etc/apache2/sites-available/

# Check the Syntax
sudo apache2ctl configtest

# Activate the config
sudo a2ensite web-localhost.conf

# Reload the server, activate the changes
sudo service apache2 reload
```
