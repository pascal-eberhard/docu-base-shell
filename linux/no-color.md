# No colors at the linux command line

The Windows Subsystem for Linux (WSL) is a nice feature. But with the default colors in the terminal, some texts are difficult to read.

So, how to deactivate the colors in output?

It seems there is no general way to do this. But the single commands have options to deactivate colors.

## ls

```bash
ls --color=never [other command parts]
```
