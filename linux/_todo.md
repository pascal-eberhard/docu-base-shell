# ffmpeg

Examples:

```bash
# Convert video file types
ffmpeg -i [inputFile].webm [outputFile]_webm.mp4

# Cut video, with timestamps, copy codecs from input file
ffmpeg -ss 00:00:00 -t 00:00:06 -i [inputFile] -acodec copy -vcodec copy -movflags faststart [outputFile]
```
