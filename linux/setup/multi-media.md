# Setup, Multi Media

Audio, Images, Video, etc.

## ffmpeg

Video analyzer, converter and editor.

```bash
sudo apt install ffmpeg
which ffmpeg
ffmpeg -version
```

## ImageMagik

Image analyzer, converter and editor.

```bash
sudo apt install imagemagick imagemagick-doc
which convert
convert -version
```
