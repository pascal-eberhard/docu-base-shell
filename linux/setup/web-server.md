# Setup, web server

Setup steps for linux web servers.

## Apache

```bash
sudo apt install apache2

which apache2
# Empty output ok

sudo service apache2 status
```

## nginx

```bash
sudo apt install nginx

which nginx
# Empty output ok

sudo service nginx status
```


