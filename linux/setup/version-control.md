# Setup, version control

## GIT

```bash
sudo apt install git
which git
git version
```

### Setup for GIT repository changes 

```bash
git config --global user.name "[your-name]"
git config --global user.email "[your-email-address]"
```

