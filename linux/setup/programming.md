# Setup, programming

Setup steps for linux programming software.

## JAVA

```bash
sudo apt install default-jdk
which java
java -version
```

## PHP 7.3

```bash
sudo apt install php7.3
sudo apt install php7.3-cli php7.3-fpm php7.3-pdo php7.3-mysql php7.3-mbstring php7.3-curl php7.3-xml php7.3-json
which php
php -v
```

### Composer

```bash
# Change to current user's home directory
cd

# Download
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"

# Install global
sudo php composer-setup.php --filename=composer --install-dir=/usr/bin
rm composer-setup.php

# Check
which composer
composer --version
```
