# Setup, common

Setup steps for common linux commands and tools.

## curl

For web and HTTP calls.

```bash
sudo apt install curl
which curl
curl --version
```

## gcc

The default linux C compiler.

```bash
sudo apt install build-essential
sudo apt install manpages-dev
which gcc
gcc --version
```

## man

The linux document command and library.

```bash
sudo apt install man
which man
man --version
```

## maria db / MySql

MySQL database software. Now Maria DB for the open source fork, after the Oracles MySQL copyright problems.

```bash
sudo apt install mariadb-server mariadb-client
sudo mysql_secure_installation
which mariadb
which mysql
mariadb --version
mysql --version
```

## vim

One of linux's default command line text editor.

```bash
sudo apt install vim
which vim
vim -v
```

## wget

Primitive web browser, only to load web content, without render it. A bit more features as curl.

```bash
sudo apt install wget
which wget
wget --version
```

