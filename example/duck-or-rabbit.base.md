# Duck or rabbit?

(This is just some funny content. An example file for demo and tests).

Check this image:

![Duck or rabbit?](https://bitbucket.org/pascal-eberhard/docu-base-shell/raw/a81da2e55db2b68c589855286d32a7bb2c0a0ed3/resources/images/duck-or-rabbit.png)

[Duck or rabbit?](https://bitbucket.org/pascal-eberhard/docu-base-shell/src/main/resources/images/duck-or-rabbit.png)
([Source](https://bitbucket.org/pascal-eberhard/docu-base-shell/src/main/resources/images/duck-or-rabbit.png.source.yml))

What do you see here, a duck or a rabbit?

## Or ..

Or even more simple: Wait until [Easter](https://en.wikipedia.org/w/index.php?title=Easter&oldid=994722126). If it brings you colorful eggs, you not only know it is a rabbit. You even know it's the Easter Bunny.

Just joking. ;)
